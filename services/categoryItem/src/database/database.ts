import { PrismaClient, categoryItem } from "@prisma/client";

const prisma = new PrismaClient();

export default prisma;
export type { categoryItem };
