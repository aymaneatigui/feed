import { Router } from "express";
import { addCategoryItem, deleteCategoryItem, getCategoryItem, updateCategoryItem } from "../middleware/categoryItem";

const routes = Router()

// CategoryItem
routes.get("/categoryItem", getCategoryItem)
routes.post("/categoryItem", addCategoryItem)
routes.put("/categoryItem", updateCategoryItem)
routes.delete("/categoryItem", deleteCategoryItem)

export default routes;