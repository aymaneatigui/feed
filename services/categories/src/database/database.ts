import { PrismaClient, categories } from "@prisma/client";

const prisma = new PrismaClient();

export default prisma;
export type { categories};
