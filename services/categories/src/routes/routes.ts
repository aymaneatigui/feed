import { Router } from "express";
import { addCategory, deleteCategory, getCategories, updateCategory } from "../middleware/categories";

const routes = Router()

// Category
routes.get("/category", getCategories)
routes.post("/category", addCategory)
routes.put("/category", updateCategory)
routes.delete("/:categoryId", deleteCategory)


export default routes;