import express from "express";
import morgan from "morgan";
import cors from "cors";
import proxy from "express-http-proxy";

const app = express();

app.use(cors());
app.use(morgan("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/api/item", proxy("http://localhost:3001"));
app.use("/api/categorie", proxy("http://localhost:3002")); 
app.use("/api/categoryItem", proxy("http://localhost:3003"));

const port = 3000;

app.listen(port, () => {
  console.log(`\n  Server :
  
    ➜  run on   :   http://localhost:${port}
    `);
});


