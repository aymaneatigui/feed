import { Router } from "express";
import { addItem, deleteItem, getItems, updateItem } from "../middleware/items";

const routes = Router()

// Items
routes.get("/item", getItems)
routes.post("/item", addItem)
routes.put("/:itemId", updateItem)
routes.delete("/:itemId", deleteItem)


export default routes;