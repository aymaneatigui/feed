#!/bin/bash
echo "Waiting for mongo to start..."
sleep 10

# Check if the replica set is already initialized
if ! mongosh --host rs-0.rs --eval 'rs.status()' | grep -q 'NotYetInitialized'; then
    echo "Setting up replica set..."
    mongosh --host rs-0.rs --eval 'rs.initiate({_id: "rs", members: [
        {_id: 0, host: "rs-0.rs:27017", priority: 1.0},
        {_id: 1, host: "rs-1.rs:27017", priority: 0.5},
        {_id: 2, host: "rs-2.rs:27017", priority: 0.5}
    ]})'
    echo "Replica set configured! ^_^"
else
    echo "Replica set already exists, skipping configuration."
fi

echo "Setting up database..."
sleep 35

# Check if the user already exists
if ! mongosh --host rs-0.rs --eval 'db.getSiblingDB("feed").getUser("docker")'; then
    mongosh --host rs-0.rs --eval 'db.getSiblingDB("feed").createUser({
            user: "docker",
            pwd: "docker",
            roles: [{ role: "readWrite", db: "feed" }]})'
    echo "Database configured! ^_^"
else
    echo "User already exists, skipping configuration."
fi

sleep 3

echo "Importing JSON files to collections..."
for file in ./app-data/json/*; do
        filename=$(basename "$file")
        collection="${filename%.*}"
        
        # Check if the collection already exists
        if ! mongosh --host rs-0.rs --eval 'db.getCollectionNames().includes("'$collection'")'; then
            mongoimport --host rs-0.rs --db feed --collection "$collection" --file "$file" --jsonArray --username docker --password docker
            echo "\nImport of $collection completed! ^_^ \n"
        else
            echo "Collection $collection already exists, skipping import."
        fi
done
